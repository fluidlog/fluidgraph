FluidGraph.prototype.chooseLoaderAndLoadD3Data = async function() {
  thisGraph = this;
  console.log("chooseLoaderAndLoadD3Data start");

  $('#loadGraphButtonId').addClass("loading");

  switch (thisGraph.config.typeServer) {
    case "home" :
      await thisGraph.loadHomeGraph();
      break;
    case "localstorage" :
      await thisGraph.loadLocalGraph();
      break;
    case "triplestore-graph":
      await thisGraph.loadTriplestoreGraph();
      break;
    case "ldp-graph":
      await thisGraph.loadLdpGraph();
      break;
    default:
  }

  $('#loadGraphButtonId').removeClass("loading");
  $('#loadGraphButtonId').hide();

  return true;
  console.log("chooseLoaderAndLoadD3Data end");
}

FluidGraph.prototype.loadHomeGraph = async function() {
  thisGraph = this;
  console.log("loadHomeGraph start");

  thisGraph.d3Data = await thisGraph.fetchConfig(thisGraph.config.dataUri);

  console.log("loadHomeGraph end");
}

FluidGraph.prototype.loadLocalGraph = async function() {
  thisGraph = this;
  console.log("loadLocalGraph start");

  if (thisGraph.config.openedGraph)
  {
    let localGraph = localStorage.getItem(thisGraph.config.version+"|"+thisGraph.config.openedGraph);

    thisGraph.d3Data = thisGraph.jsonD3ToD3Data(localGraph);
    thisGraph.graphName = thisGraph.config.openedGraph;
    thisGraph.changeGraphName();
    // Je pense qu'il faudra un jour garder ça uniquement dans fludy... et afficher ici le graph
  }

  console.log("loadLocalGraph end");
}

FluidGraph.prototype.loadTriplestoreGraph = async function() {
  thisGraph = this;
  let optionLabel;
  let optionPairLabel;
  let method;
  let response;
  let triples;

  if (thisGraph.config.debug) console.log("loadTriplestoreGraph start");

  console.log('Début de récupération des données de Semapps...');

  const query = `
  CONSTRUCT {
    ?s0 ?p0 ?o0
  }
  WHERE
  {
    ?s0 ?p0 ?o0 .
  }`;

  // Spec du fetch : https://developer.mozilla.org/fr/docs/Web/API/Fetch_API/Using_Fetch
  // Vidéo de Grafikart : https://www.youtube.com/watch?v=B2Jt9bNMxyw
  // TypeError : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/TypeError
  if (thisGraph.config.modeRequest == "sparql")
  {
    method = {
      method: 'POST',
      headers: {
        'accept': 'application/ld+json',
      },
      body: query
    };
  }
  else { // ldp
    method = {
      method: 'GET',
      headers: {
        'accept': 'application/ld+json',
      }
    };
  }

  try{
    let response = await fetch(thisGraph.config.dataUri, method);

    if (response.ok){
      triples = await response.json();
      console.log('Fin de récupération des données de Semapps...');

      console.log('Début de conversion des données dans D3Data...');
      thisGraph.d3Data = await thisGraph.jsonLdObjectToD3Data(triples);
//      console.log(thisGraph.d3Data);
      console.log('Fin de conversion des données dans D3Data...');

      if (thisGraph.config.displayStatistics)
      {
        console.log("Début d'affichage des stats...");
        thisGraph.updateStatistics(thisGraph.statisticsD3Data);
        console.log("Fin d'affichage des stats...");
      }
    }
    else {
      console.log("Erreur lors de la récupération des données : ", response.status)
      $("#alertModalFetch").modal('show');
      thisGraph.d3Data = thisGraph.backupD3Data;
      await thisGraph.displayGraph();
    }
  }
  catch (e){
    console.error("Erreur lors de la récupération des données : "+e);
    $("#alertModalFetch").modal('show');
    thisGraph.d3Data = thisGraph.backupD3Data;
    await thisGraph.displayGraph();
  }

  if (thisGraph.config.debug) console.log("loadTriplestoreGraph end");
}

FluidGraph.prototype.downloadGraph = function() {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("downloadGraph start");

  let jsonD3 = thisGraph.d3DataToJsonD3();
  let blob = new Blob([jsonD3], {type: "text/plain;charset=utf-8"});
  let now = new Date();
  let date_now = now.getDate()+"-"+now.getMonth()+1+"-"+now.getFullYear()+"-"+now.getHours()+":"+now.getMinutes()+":"+now.getSeconds();
  saveAs(blob, "Carto-"+thisGraph.graphName+"-"+date_now+".d3json");

  if (thisGraph.config.debug) console.log("downloadGraph end");
}

FluidGraph.prototype.uploadGraph = function(input) {
thisGraph = this;

if (thisGraph.config.debug) console.log("uploadGraph start");

  if (window.File && window.FileReader && window.FileList && window.Blob) {
    let uploadFile = input[0].files[0];
    let filereader = new window.FileReader();

    filereader.onload = function(){
      let txtRes = filereader.result;
      // TODO better error handling
      try{
        thisGraph.clearGraph();
        thisGraph.d3Data = thisGraph.jsonD3ToD3Data(txtRes);
        thisGraph.changeGraphName();
        thisGraph.initializeDisplay();
        thisGraph.initializeSimulation();
        thisGraph.movexy();
        thisGraph.saveGraphToLocalStorage();
        thisGraph.setOpenedGraph();
      }catch(err){
        window.alert("Error parsing uploaded file\nerror message: " + err.message);
        return;
      }
    };
    filereader.readAsText(uploadFile);
    $("#sidebarButton").click();

  } else {
    alert("Your browser won't let you save this graph -- try upgrading your browser to IE 10+ or Chrome or Firefox.");
  }

  if (thisGraph.config.debug) console.log("uploadGraph end");
}

FluidGraph.prototype.loadLdpGraph = function() {
  if (thisGraph.config.debug) console.log("loadLdpGraph start");

  $("#message").html("<p>Si la carto ne s'affichent pas, plusieurs raisons possibles :"
  + "<ul><li>Soit le serveur de base de données n'est pas disponible : "
  + "dans ce cas envoyez un email à <a href='mailto:contact@assemblee-virtuelle.org'>contact@assemblee-virtuelle.org</a></li>"
  + "<li>Acceptez l'exception de sécurité qui apparait en cliquant sur "
  +" <a href='https://ldp.virtual-assembly.org:8443/'>ce lien</a> jusqu'à obtenir un logo 'stample',"
  +"</li></p>").show();

  //If there's a hash, remove it to put the new one
  window.location.hash = thisGraph.config.dataUri;

  store.get(thisGraph.config.dataUri).then(function(externalGraph){
    if (externalGraph.nodes)
    {
      $("#message").hide();
      thisGraph.d3Data = thisGraph.jsonLdToD3Data(externalGraph);
      thisGraph.changeGraphName();
      thisGraph.ldpGraphName = thisGraph.config.dataUri;
      if (thisGraph.selectedLdpGraphName)
        thisGraph.updateSelectOpenGraphModalPreview();

      thisGraph.initializeAndDisplayGraph();

      if (thisGraph.config.debug) console.log("loadLdpGraph end");
      return true;
    }
    else if (externalGraph["@id"])
    {
      thisGraph.createFluidGraph(externalGraph);
      thisGraph.initializeAndDisplayGraph();
    }
    return false;
  });
  return false;
}
