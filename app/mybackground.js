//rescale g
FluidGraph.prototype.rescale = function(){
  if (thisGraph.config.debug) console.log("rescale start");

  if (thisGraph.config.allowDragOnBg)
  {
    /* V3
    thisGraph.bgElement.attr("transform",
      "translate(" + d3.event.translate + ")"
      + " scale(" + d3.event.scale + ")");
      */
    // V4
    thisGraph.currentTransformZoom = d3.event.transform;
    // Attention à bien choisir le bon div, pour corriger le problème de saut de déplacement !
    // var select_svg_g = d3.select("#svg_g"); // #svg_g
    // var svg_g = thisGraph.svg_g; // #bg_chart
    // var bgElement = thisGraph.bgElement; // #bg_chart
    thisGraph.bgElement.attr("transform", thisGraph.currentTransformZoom);
  }

  if (thisGraph.config.debug) console.log("rescale end");
}

//pan g / Not used for the moment
// FluidGraph.prototype.bgPan = function(direction){
//   if (thisGraph.config.debug) console.log("rescale start");
//
//   var translateCoords, getTransform;
//
//   if (thisGraph.panTimer) {
//     clearTimeout(thisGraph.panTimer);
//
//     getTransform = thisGraph.bgElement.attr("transform");
//     translateCoords = d3.transform(getTransform);
//     if (direction == 'left')
//     {
//       thisGraph.translateX = translateCoords.translate[0] + thisGraph.panBgSpeed;
//       thisGraph.translateY = translateCoords.translate[1];
//     }
//     else if  (direction == 'right')
//     {
//       thisGraph.translateX = translateCoords.translate[0] - thisGraph.panBgSpeed;
//       thisGraph.translateY = translateCoords.translate[1];
//     }
//     else if  (direction == 'up')
//     {
//       thisGraph.translateX = translateCoords.translate[0];
//       thisGraph.translateY = translateCoords.translate[1] + thisGraph.panBgSpeed;
//     }
//     else if (direction == 'down')
//     {
//       thisGraph.translateX = translateCoords.translate[0];
//       thisGraph.translateY = translateCoords.translate[1] - thisGraph.panBgSpeed;
//     }
//     thisGraph.bgElement.transition().attr("transform",
//                     "translate(" + thisGraph.translateX + "," + thisGraph.translateY + ")");
//
//     thisGraph.panTimer = setTimeout(function() {
//       thisGraph.bgPan(direction);
//     }, 50);
//   }
//
//   if (thisGraph.config.debug) console.log("rescale end");
// }

FluidGraph.prototype.bgOnMouseDown = function(d){
  thisGraph = this;

  if (thisGraph.config.debug) console.log("bgOnMouseDown start");

  if (thisGraph.state.selectedLink){
    thisGraph.removeSelectFromLinks();
  }

  if (thisGraph.state.selectedNode && thisGraph.state.svgMouseDownNode){
    thisGraph.fixUnfixNode(thisGraph.state.svgMouseDownNode,thisGraph.state.selectedNode);
  }

  //If it still exist somthing "selected", set to "unselected"
  d3.selectAll("#path.selected").classed(thisGraph.consts.selectedClass, false);
  d3.selectAll("#nodecircle.selected").classed(thisGraph.consts.selectedClass, false);

  if (thisGraph.state.editedNode)
  {
    // var el_edited_node = d3.select(thisGraph.state.editedNode);
    // var el_edited_node_label = el_edited_node.select("#fo_content_edited_node_label");
    // if (el_edited_node_label.node())
      thisGraph.closeNode.call(thisGraph, "edited");
  }

  if (thisGraph.state.openedNode)
  {
    // var el_opened_node = d3.select(thisGraph.state.openedNode);
    // var el_closed_node_label = el_opened_node.select("#fo_content_closed_node_label");
    // if (el_closed_node_label.node())
      thisGraph.closeNode.call(thisGraph, "opened");
  }

  if (thisGraph.state.editedLinkLabel)
  {
    thisGraph.saveEditedLinkLabel.call(thisGraph)
  }

  if (thisGraph.config.debug) console.log("bgOnMouseDown start");
}

FluidGraph.prototype.bgOnMouseOver = function(d){
  thisGraph = this;

  if (thisGraph.config.debug) console.log("bgOnMouseOver start");

  var xycoords = d3.mouse(thisGraph.bgElement.node());

  // get coords of mouseEvent relative to svg container to allow for panning
  // relCoords = d3.mouse($('svg').get(0));
  // if (relCoords[0] < thisGraph.panBgLRBoundary) {
  //     thisGraph.panTimer = true;
  //     thisGraph.bgPan("left");
  // } else if (relCoords[0] > ($('svg').width() - thisGraph.panBgLRBoundary)) {
  //     thisGraph.panTimer = true;
  //     thisGraph.bgPan('right');
  // } else if (relCoords[1] < thisGraph.panBgUDRBoundary) {
  //     thisGraph.panTimer = true;
  //     thisGraph.bgPan('up');
  // } else if (relCoords[1] > ($('svg').height() - thisGraph.panBgUDRBoundary)) {
  //     thisGraph.panTimer = true;
  //     thisGraph.bgPan('down');
  // } else {
  //     try {
  //         clearTimeout(thisGraph.panTimer);
  //     } catch (e) {
  //
  //     }
  // }

  // if the origin click is not a node, then pan the graph (activated by bgOnMouseDown)...
  if (!thisGraph.state.mouseDownNode) return;

  if (thisGraph.config.debug) console.log("bgOnMouseOver end")
}

FluidGraph.prototype.bgOnMouseMove = function(d){
  thisGraph = this;

  if (thisGraph.config.debug) console.log("bgOnMouseMove start");

  // var xycoords = d3.mouse(thisGraph.bgElement.node());
  //
  // // get coords of mouseEvent relative to svg container to allow for panning
  // relCoords = d3.mouse($('svg').get(0));
  // if (relCoords[0] < thisGraph.panBgLRBoundary) {
  //     thisGraph.panTimer = true;
  //     thisGraph.bgPan("left");
  // } else if (relCoords[0] > ($('svg').width() - thisGraph.panBgLRBoundary)) {
  //     thisGraph.panTimer = true;
  //     thisGraph.bgPan('right');
  // } else if (relCoords[1] < thisGraph.panBgUDRBoundary) {
  //     thisGraph.panTimer = true;
  //     thisGraph.bgPan('up');
  // } else if (relCoords[1] > ($('svg').height() - thisGraph.panBgUDRBoundary)) {
  //     thisGraph.panTimer = true;
  //     thisGraph.bgPan('down');
  // } else {
  //     try {
  //         clearTimeout(thisGraph.panTimer);
  //     } catch (e) {
  //
  //     }
  // }

  // if the origin click is not a node, then pan the graph (activated by bgOnMouseDown)...
  if (!thisGraph.state.mouseDownNode) return;

  if (thisGraph.config.debug) console.log("bgOnMouseMove end")
}

FluidGraph.prototype.bgOnMouseUp = function(d){
  var thisGraph = this;

  if (thisGraph.config.debug) console.log("bgOnMouseUp start");

  console.log("bgOnMouseUp1 : d3.event.sourceEvent.target", d3.event.target);
  console.log("bgOnMouseUp2 : $('#fo_i_edit_image')[0]", $("#fo_i_edit_image")[0]);
  if (d3.event.target === $("#fo_i_edit_image")[0])
     return
  if (d3.event.target === $("#fo_i_center_image")[0])
    return
  if (d3.event.target === $("#fo_i_focus_image")[0])
     return
  if (d3.event.target === $("#fo_i_hypertext_image")[0])
   return
  if (d3.event.target === $("#fo_i_delete_image")[0])
  return
  if (d3.event.target === $("#edit_select_node_type")[0])
   return

  if (!thisGraph.state.mouseDownNode)
  {
    thisGraph.resetMouseVars();
    return;
  }

  var xycoords = d3.mouse(thisGraph.bgElement.node());

  if (thisGraph.config.debug) console.log("bgOnMouseUp end");
}

// From https://github.com/cjrd/directed-graph-creator/blob/master/graph-creator.js
FluidGraph.prototype.bgKeyDown = function() {
  var thisGraph = this;

  if (thisGraph.config.debug) console.log("bgKeyDown start");

  // make sure repeated key presses don't register for each keydown
  if(thisGraph.state.lastKeyDown !== -1) return;

  thisGraph.state.lastKeyDown = d3.event.keyCode;

  switch(d3.event.keyCode) {
  case thisGraph.consts.BACKSPACE_KEY:
  break;
  case thisGraph.consts.DELETE_KEY:
    d3.event.preventDefault();
    if (thisGraph.state.selectedNode){
      // Revoir cette fonction deleteNodeFromD3Nodes qui supprime des index de la liste...
      thisGraph.d3Data.nodes = thisGraph.deleteNodeFromD3Nodes(thisGraph.state.selectedNode["@id"]);
      //delete edges linked to this (old) node
      // Revoir cette fonction deleteEdgesOfNodeFromD3Edges qui supprime des index de la liste...
      thisGraph.d3Data.edges = thisGraph.deleteEdgesOfNodeFromD3Edges(thisGraph.state.selectedNode["@id"]);
      thisGraph.resetStateNode();
      thisGraph.initializeDisplay();
      thisGraph.initializeForces();
    } else if (thisGraph.state.selectedLink){
      if (thisGraph.config.allowModifyLink == true)
      {
        thisGraph.deleteLink()
      }
    }
    break;
  }

  if (thisGraph.config.debug) console.log("bgKeyDown end");
}

FluidGraph.prototype.bgKeyUp = function() {
  this.state.lastKeyDown = -1;
};
