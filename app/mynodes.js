// Prototypes concerning nodes

FluidGraph.prototype.drawNodes = function(svgNodes) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("drawNodes start");

  var rectCircle;
  rectCircle = svgNodes
    .append("rect")
    .attr("id", "nodecircle")
    .attr("class", "nodecircle")
    .attr("x", function (d){
      var radius = thisGraph.getProportionalRadius(d);
      var x = -radius;
      return x;
    })
    .attr("y", function (d){
      var radius = thisGraph.getProportionalRadius(d);
      var y = -radius;
      return y;
    })
    .attr("width", function (d){
      var radius = thisGraph.getProportionalRadius(d);
      var width = radius*2;
      return width;
    })
    .attr("height", function (d){
      var radius = thisGraph.getProportionalRadius(d);
      var height = radius*2;
      return height;
    })
    .attr("rx", thisGraph.customNodes.curvesCornersClosedNode)
    .attr("ry", thisGraph.customNodes.curvesCornersClosedNode)
    // .style("fill", function(d) {
    //   return thisGraph.customNodes.colorType[d["@type"]];
    // })
    // .style("stroke", function(d) {
    //     if (d.fixed == true)
    //       d.fixed = false;
    //
    //   return thisGraph.customNodes.strokeColorType[d["@type"]];
    // })
    // .style("stroke-width", thisGraph.customNodes.strokeWidth)
    //
    // background: linear-gradient( #555, #2C2C2C);
    // .style("stroke-opacity", thisGraph.customNodes.strokeOpacity)
    // .style("cursor", thisGraph.customNodes.cursor)
    // .style("opacity", 1)

    .attr("style",function(d) {
      var style =
                  "fill:" + thisGraph.customNodes.colorType[d["@type"]] + ";"
                  +"stroke:"+ thisGraph.customNodes.strokeColorType[d["@type"]] + ";"
                  +"stroke-width:"+ thisGraph.customNodes.strokeWidth + ";"

      return style;
    })

    .style("stroke-opacity", thisGraph.customNodes.strokeOpacity)
    .style("cursor", thisGraph.customNodes.cursor)
    .style("opacity", 1)

  if (thisGraph.customNodes.displayIndex)
    thisGraph.displayIndex(svgNodes)

  if (thisGraph.customNodes.displayType)
    thisGraph.displayType(svgNodes)

  if (thisGraph.customNodes.displayText)
    thisGraph.displayText(svgNodes)

  if (thisGraph.config.debug) console.log("drawNodes end");
}

FluidGraph.prototype.displayText = function(svgNodes) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("displayText start");

  var fo_content_closed_node_label = svgNodes
    .append("foreignObject")
    .attr("id", "fo_content_closed_node_label")
    .attr("x", -thisGraph.customNodesText.widthMax / 2)
    .attr("y", -thisGraph.customNodesText.heightMax / 2)
    .attr("width", thisGraph.customNodesText.widthMax)
    .attr("height", thisGraph.customNodesText.heightMax)

  //fo xhtml
  var fo_xhtml_content_closed_node_label = fo_content_closed_node_label
    .append('xhtml:div')
    .attr("class", "fo_xhtml_content_closed_node_label")
    .attr("style", "width:"+thisGraph.customNodesText.widthMax+"px;"
                  +"height:"+thisGraph.customNodesText.heightMax+"px;")

  //label_closed_node
  var label_closed_node = fo_xhtml_content_closed_node_label
    .append("div")
    .attr("id", "label_closed_node")
    .attr("class", "label_closed_node")
    .attr("style", function(d) {
      var color = thisGraph.customNodes.neighbourColorTypeRgba[d["@type"]];
      var style = "background-color:rgba(" + color
                                  + "," + thisGraph.customNodesText.strokeOpacity + ");"
                                  + "border: 1px solid rgba("
                                  + color + ","
                                  + thisGraph.customNodesText.strokeOpacity + ");"
                                  + "-moz-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                                  + "-webkit-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                                  + "border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"

      // Supprimé lors de la suppression du editGraphMode
      // https://hackmd.lescommuns.org/CwIwhgDAHBAmCmBaATNAZo48CMVFQGYCNYRk0CIB2AVlgNmCA===?both
      if (d.hypertext != undefined)
        style += "cursor:pointer;"
      else
         style += "cursor:" + thisGraph.customNodes.cursor + ";"

      return style;
    })
    .html(function(d, i) {
      var label = "";
      if (typeof d.label != "undefined")
      {
        if (d.label.length > thisGraph.customNodes.maxCharactersInLabel)
          label = d.label.substring(0,thisGraph.customNodes.maxCharactersInLabel)+" ...";
        else {
          label = d.label;
        }
      }
      // Supprimé lors de la suppression du editGraphMode
      // https://hackmd.lescommuns.org/CwIwhgDAHBAmCmBaATNAZo48CMVFQGYCNYRk0CIB2AVlgNmCA===?both
      if (thisGraph.config.editGraphMode == false
        && thisGraph.config.clicOnNodeAction == "media"
        && d.hypertext != undefined)
      {
        return "<a id='a_closed_node_hypertext' href='"+d.hypertext+"' target='_blank'>"+label+"</a>";
      }
      else {
         return label;
      }
    })

  //Rect to put events
  var fo_content_closed_node_events = svgNodes
    .append("foreignObject")
    .attr("id", "fo_content_closed_node_events")
    .attr("x", -thisGraph.customNodesText.widthMax / 2)
    .attr("y", -thisGraph.customNodesText.heightMax / 2)
    .attr("width", thisGraph.customNodesText.widthMax)
    .attr("height", thisGraph.customNodesText.heightMax)

  var fo_xhtml_content_closed_node_events = fo_content_closed_node_events
    .append('xhtml:div')
    .attr("class", "fo_xhtml_content_closed_node_events")
    .attr("style", function (d){
      var radius = thisGraph.getProportionalRadius(d);
      var marginLeft = (thisGraph.customNodesText.widthMax - radius*2)/2
      var style = "margin-left:" + marginLeft + "px;"
                  + "padding:" + radius + "px;"
                  + "width:"+ radius*2 + "px;"
                  + "height:"+ radius*2 + "px;position:static;"

      // Supprimé lors de la suppression du editGraphMode
      // https://hackmd.lescommuns.org/CwIwhgDAHBAmCmBaATNAZo48CMVFQGYCNYRk0CIB2AVlgNmCA===?both
      // if (thisGraph.config.editGraphMode == false && d.hypertext != undefined)
      //   style += "cursor:pointer;"
      // else
      //   style += "cursor:" + thisGraph.customNodes.cursor + ";"

      return style;
    })

    .on("mousedown",function(d){
      thisGraph.nodeOnMouseDown.call(thisGraph, d3.select(this.parentNode.parentNode), d)
    })
    .on("click",function(d){
      thisGraph.nodeOnMouseUp.call(thisGraph, d3.select(this.parentNode.parentNode), d)
    })
    .on("mouseover.fade", function(d){
      if (!thisGraph.state.filterTypeMode)
      {
        thisGraph.highlightFocusContext.call(thisGraph, d, 0.3)
      }
    })
    .on("mouseout.fade", function(d){
      if (!thisGraph.state.filterTypeMode)
      {
        thisGraph.highlightFocusContext.call(thisGraph, d, 1)
      }
    })
    .on("mouseover",function(d){
      //console.log("mouseover on node events")
      if (d3.select(this.parentNode.parentNode) !== thisGraph.state.openedNode)
      {
        thisGraph.nodeOnMouseOver.call(thisGraph, d3.select(this.parentNode.parentNode), d)
      }
    })
    .on("mouseout",function(d){
      //console.log("mouseout on node events")
      if (d3.select(this.parentNode.parentNode) !== thisGraph.state.openedNode)
      {
        thisGraph.nodeOnMouseOut.call(thisGraph, d3.select(this.parentNode.parentNode), d)
      }
    })
    .on("dblclick",function(d){
        if (thisGraph.config.editWithDoubleClick == true)
          thisGraph.editNode.call(thisGraph, d3.select(this.parentNode.parentNode.parentNode), d);
    })

  if (thisGraph.config.debug) console.log("displayText end");
}

FluidGraph.prototype.displayIndex = function(svgNodes) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("displayIndex start");

  /* id circle */
  svgNodes
    .append("circle")
    .attr("id", "circle_index")
    .attr("class", "circle_index")
    .attr("cx", thisGraph.nodeIndexCircle.cxClosed)
    .attr("cy", thisGraph.nodeIndexCircle.cyClosed)
    .attr("r", thisGraph.nodeIndexCircle.r)
    .attr("fill", function(d) {
      return thisGraph.customNodes.colorType[d["@type"]];
    })

  /* Text of id */
  svgNodes
    .append("text")
    .attr("id", "text_index")
    .attr("class", "text_index")
    .attr("dx", thisGraph.nodeIndexCircle.dxClosed-8)
    .attr("dy", thisGraph.nodeIndexCircle.dyClosed)
    .attr("fill", "#555")
    .attr("font-weight", "bold")
    .text(function(d) {
      var typeOfIndex;
      if (thisGraph.customNodes.typeOfIndexToDisplay == "i")
        typeOfIndex = d.i+" ["+d.neighbours.length+"]";
      else
        typeOfIndex = d.index+" ["+d.neighbours.length+"]";
      return typeOfIndex;
    })

  if (thisGraph.config.debug) console.log("displayIndex end");
}

FluidGraph.prototype.displayType = function(svgNodes) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("displayType start");

  /* type circle*/
  svgNodes
    .append("circle")
    .attr("id", "circle_type")
    .attr("class", "circle_type")
    .attr("cx", 0)
    .attr("cy", thisGraph.nodeTypeIcon.cyClosed)
    .attr("r", thisGraph.nodeTypeIcon.r)

  /* Image of type */
  var fo_type_image = svgNodes
    .append("foreignObject")
    .attr("id", "fo_type_image")
    .attr('x', thisGraph.nodeTypeIcon.xClosed)
    .attr('y', thisGraph.nodeTypeIcon.yClosed)
    .attr('width', 25)
    .attr('height', 25)

  //xhtml div image
  var fo_xhtml_type_image = fo_type_image
    .append('xhtml:div')
    .attr("id", "fo_div_type_image")
    .attr("class", "fo_div_image")
    .append('i')
    .attr("id", "fo_i_type_image")
    .attr("class", function(d) {
      return "ui large disabled " + thisGraph.customNodes.imageType[d["@type"]] + " icon";
    })
    .attr("style", "display:inline")

  if (thisGraph.config.debug) console.log("displayType end");
}

FluidGraph.prototype.changeIndexNode = function(node,index) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("changeIndexNode start");

  var el = d3.select(node);
  var text_index = el.select("#text_index");
  text_index.text(index);

  if (thisGraph.config.debug) console.log("changeIndexNode end");
}

FluidGraph.prototype.changeTypeNode = function(node,type) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("displayType start");

  var el = d3.select(node);

  var nodecircle = el.select("#nodecircle");
  nodecircle.style("fill", thisGraph.customNodes.colorType[type]);
  nodecircle.style("stroke", thisGraph.customNodes.strokeColorType[type]);

  var type_el = el.select("#fo_type_image");
  type_el.select('#fo_i_type_image').remove();
  type_el.select('#fo_div_type_image')
      .append('i')
        .attr("id", "fo_i_type_image")
        .attr("class", "ui large " + thisGraph.customNodes.imageType[type] + " icon")
      .attr("style", "display:inline")

  var circle_index_el = el.select("#circle_index");
  circle_index_el.style("fill", function(d) { return thisGraph.customNodes.colorType[type] } );

  if (thisGraph.config.debug) console.log("displayType end");
}

FluidGraph.prototype.addNodeOnBg = function(thisGraph) {
  //Warning, here, we need "this" for mouse coord

  if (thisGraph.config.debug) console.log("addNodeOnBg start");

  var newNode = thisGraph.addDataNode.call(this, thisGraph);
  thisGraph.initializeDisplay();
  thisGraph.initializeSimulation();

  if (!thisGraph.config.force.enabled)
    thisGraph.movexy();

  var d3Node = thisGraph.getD3NodeFromIdentifier(newNode["@id"]);
  thisGraph.editNode.call(thisGraph, d3Node, newNode);

  if (thisGraph.config.debug) console.log("addNodeOnBg end");

  return newNode;
}

FluidGraph.prototype.addDataNode = function(thisGraph, newNode) {
  //Warning, here, we need "this" for mouse coord

  if (thisGraph.config.debug) console.log("addDataNode start");
  var xy = [];

  if (typeof this.__ondblclick != "undefined") //if after dblclick
  {
    xy = d3.mouse(this);
  } else {
    if (typeof newNode.x == "undefined")
    {
      xy[0] = thisGraph.config.xNewNode;
      xy[1] = thisGraph.config.yNewNode;
    }
  }

  //We check values cause this function can be call externally...
  if (typeof newNode == "undefined")
    var newNode = {}

  if (typeof newNode.label == "undefined")
    newNode.label = thisGraph.customNodes.blankNodeLabel;
  if (typeof newNode.hypertext == "undefined")
    newNode.hypertext = "";
  if (typeof newNode["@type"] == "undefined")
    newNode["@type"] = thisGraph.customNodes.blankNodeType;
  if (typeof newNode["@id"] == "undefined")
    newNode["@id"] = thisGraph.customNodes.uriBase + thisGraph.getNodeIndex();
  if (typeof newNode.index == "undefined")
    newNode.index = thisGraph.getNodeIndex();

  if (typeof newNode.px == "undefined")
    newNode.px = xy[0];
  if (typeof newNode.py == "undefined")
    newNode.py = xy[1];
  if (typeof newNode.x == "undefined")
    newNode.x = xy[0];
  if (typeof newNode.y == "undefined")
    newNode.y = xy[1];

  thisGraph.d3Data.nodes.push(newNode)

  if (thisGraph.config.debug) console.log("addDataNode end");
  return newNode;
}

FluidGraph.prototype.drawNewNode = function(sourceNode, targetNode) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("drawNewNode start");

  // thisGraph.OrderNodesInSvgSequence();

  thisGraph.initializeDisplay();
  thisGraph.initializeSimulation();

  var d3Node = thisGraph.getD3NodeFromIdentifier(targetNode["@id"]);
  d3Node
      .attr("transform", function (d){return "translate("+sourceNode.x+","+sourceNode.y+")"})
      .transition()
      .duration(thisGraph.customNodes.transitionDurationComeBack)
      .attr("transform", function (d){return "translate("+targetNode.x+","+targetNode.y+")"})
      .on("end", function() {
        thisGraph.svgNodesEnter = thisGraph.bgElement.selectAll("#node")
        				              .data(thisGraph.d3Data.nodes, function(d) { return d.index;})

        thisGraph.bgElement.selectAll("#node").attr("transform", function(d) {
          return "translate(" + d.x + "," + d.y + ")";
        });
      });

  if (thisGraph.config.debug) console.log("drawNewNode end");
}

FluidGraph.prototype.closeNode = function(typeOfNode) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("closeNode start");

  if (typeOfNode == "edited")
  {
    var el = d3.select(thisGraph.state.editedNode);
    var p_el = d3.select(thisGraph.state.editedNode.parentNode);

    var edit_node_input_index = el.select("#edit_node_input_index");
    var edit_node_select_type = el.select("#edit_node_select_type");
    var edit_node_textarea_label = el.select("#edit_node_textarea_label");
    var edit_node_input_hypertext = el.select("#edit_node_input_hypertext");

    if (thisGraph.customNodes.displayIndex)
    {
      var integer_input_index = parseInt(input_index.node().value, 10);
      if (integer_input_index)
        thisGraph.state.editedNode.__data__.index = integer_input_index;
      else
        thisGraph.state.editedNode.__data__.index = thisGraph.state.editedIndexNode;
    }

    if (edit_node_select_type.node())
      thisGraph.state.editedNode.__data__["@type"] = edit_node_select_type.node().value;

    if (edit_node_textarea_label.node())
      thisGraph.state.editedNode.__data__.label = edit_node_textarea_label.node().value;
    else
      thisGraph.state.editedNode.__data__.label = thisGraph.customNodes.blankNodeLabel;

    if (edit_node_input_hypertext.node())
      thisGraph.state.editedNode.__data__.hypertext = edit_node_input_hypertext.node().value;

    thisGraph.saveEditNode();

    el.select("#fo_content_edited_node_label").remove();
    thisGraph.displayText(el);

    thisGraph.state.editedNode = null;
  }
  else { //opened
      var el = d3.select(thisGraph.state.openedNode);

      el.select("#fo_content_opened_node").remove();
      el.select("#fo_content_opened_node_events").remove();
      el.select("#fo_xhtml_content_open_node_label").remove();

      d3.select("#fo_node_neighbour").remove();

      // el.select("#fo_content_closed_node_label")
      //   .transition()
      //   .duration(thisGraph.customNodes.transitionDurationClose)
      //   .attr("y", -thisGraph.customNodesText.heightMax / 2)

      if (thisGraph.config.clicOnNodeAction == "flod")
      {
        thisGraph.displayText(el);

        if (thisGraph.config.displayFlodPanel)
        {
          d3.select("#flod_panel_content").remove();
          let flodPanelDiv = d3.select('#flodPanelId')
          flodPanelDiv.attr("style","")
          flodPanelDiv.append('div')
          .attr("id", "flod_panel_content")
          .append('div')
          .attr("id", "flod_panel_content_label")
          .text(thisGraph.config.firstLabelFlodPanel)
        }
      }

      thisGraph.state.openedNode = null;
  }

  el
    .select("#circle_index")
    .transition()
    .duration(thisGraph.customNodes.transitionDurationClose)
    .attr("cy", thisGraph.nodeIndexCircle.cyClosed)

  el
    .select("#text_index")
    .transition()
    .duration(thisGraph.customNodes.transitionDurationClose)
    .attr("dy", thisGraph.nodeIndexCircle.dyClosed)

  el
    .select("#circle_type")
    .transition()
    .duration(thisGraph.customNodes.transitionDurationClose)
    .attr("cy", thisGraph.nodeTypeIcon.cyClosed)

  el
    .select("#fo_type_image")
    .transition()
    .duration(thisGraph.customNodes.transitionDurationClose)
    .attr("y", thisGraph.nodeTypeIcon.yClosed)

  el.select("#nodecircle")
    .transition()
    .duration(thisGraph.customNodes.transitionDurationClose)
    .attr("x", function (d){
      var radius = thisGraph.getProportionalRadius(d);
      var x = -radius;
      return x;
    })
    .attr("y", function (d){
      var radius = thisGraph.getProportionalRadius(d);
      var y= -radius;
      return y;
    })
    .attr("width", function (d){
      var radius = thisGraph.getProportionalRadius(d);
      var width = radius*2;
      return width;
    })
    .attr("height", function (d){
      var radius = thisGraph.getProportionalRadius(d);
      var height = radius*2;
      return height;
    })
    .attr("rx", thisGraph.customNodes.curvesCornersClosedNode)
    .attr("ry", thisGraph.customNodes.curvesCornersClosedNode)

//    thisGraph.OrderNodesInSvgSequence();

  if (thisGraph.config.debug) console.log("closeNode end");
}

FluidGraph.prototype.saveEditNode = function() {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("saveEditNode start");

  var editedNodeData = thisGraph.state.editedNode.__data__;
  thisGraph.changeIndexNode(thisGraph.state.editedNode,editedNodeData.index);
  thisGraph.changeTypeNode(thisGraph.state.editedNode,editedNodeData["@type"]);

  if (thisGraph.config.debug) console.log("saveEditNode end");
}

FluidGraph.prototype.nodeOnMouseOver = function(d3Node, dNode) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("nodeOnMouseOver start");

  if (d3.event.defaultPrevented) return;

  d3.event.stopPropagation();

  if (thisGraph.config.repulseNeighbourOnHover) {
    var el = d3.select(d3Node.node());
    el.repulseNeighbour.call(thisGraph, dNode, 100);
  }

  if (thisGraph.state.draggingNode
      && dNode
      && dNode != thisGraph.state.draggingNode)
  {
    thisGraph.state.targetNode = dNode;
    thisGraph.state.targetNodeSvg = d3Node;
  }

  if (thisGraph.config.debug) console.log("nodeOnMouseOver end");
}

FluidGraph.prototype.nodeOnMouseOut = function(d3NodeTarget, dNodeTarget) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("nodeOnMouseOut start");

  if (thisGraph.config.repulseNeighbourOnHover) {
    var el = d3.select(d3Node.node());
    el.repulseNeighbour.call(thisGraph, dNodeTarget, 1);
  }

  if (thisGraph.state.draggingNode)
  {
    thisGraph.state.targetNode = null;

    if (thisGraph.state.openedNode && thisGraph.config.editGraphMode)
    {
// Connecteur Supprimé
    }
    else
    {
      thisGraph.removeSelectFromNode(dNodeTarget);
      thisGraph.removeSelectFromNode(thisGraph.state.draggingNode);
      if (thisGraph.state.targetNodeSvg)
      {
        thisGraph.state.targetNodeSvg.select("#nodecircle").style("opacity","1");
      }
    }
    thisGraph.state.targetNodeSvg = null;
  }

  if (thisGraph.config.debug) console.log("nodeOnMouseOut end");
}

d3.selection.prototype.repulseNeighbour = function(d, weight) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("repulseNeighbour start");

  d.weight = weight;

  if (thisGraph.config.debug) console.log("repulseNeighbour end");
}

FluidGraph.prototype.focusContextNodeOn = async function(dNode) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("focusContextNodeOn start");

  thisGraph.state.focusMode = true;
  thisGraph.d3DataFc = thisGraph.getNeighbourNodesAndLinks(dNode);
  console.log(thisGraph.d3DataFc);

  $('#searchId').hide();

  thisGraph.config.activeElasticityOnDragNode = true;
  thisGraph.config.clicOnNodeAction = "options"; // options, flod, media
  await thisGraph.initializeNodesAndEdges();
  await thisGraph.displayGraph();
  thisGraph.centerNode(dNode);
//  var d3Node = thisGraph.getD3NodeFromIdentifier(dNode["@id"]);
//  thisGraph.openNode(d3Node, dNode);

  if (thisGraph.config.debug) console.log("focusContextNodeOn end");
}

FluidGraph.prototype.focusContextNodeOff = async function(dNode) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("focusContextNodeOff start");

  $('#displayGraphLoaderId').addClass("active");
  // // force the sleep, to activate loader... (?)
  await thisGraph.sleep(.1);

  //Vidage de l'objet contenant les données de focus/context
  thisGraph.d3DataFc = null;
  $('#searchId').show();

  thisGraph.config.activeElasticityOnDragNode = false;
  thisGraph.config.clicOnNodeAction = "flod"; // options, flod, media
  await thisGraph.initializeNodesAndEdges();
  await thisGraph.displayGraph();

  $('#displayGraphLoaderId').removeClass("active");

  let d3Node = thisGraph.getD3NodeFromIdentifier(dNode["@id"])
  thisGraph.centerNode(dNode);
  thisGraph.openNode(d3Node, dNode);
  thisGraph.state.focusMode = false;

  if (thisGraph.config.debug) console.log("focusContextNodeOff end");
}

FluidGraph.prototype.fixUnfixNode = function(d3Node, d) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("fixUnfixNode start");

  if (d3.event.defaultPrevented) return;

  var status;

  if (d.fixed == true) {
    thisGraph.removeSelectFromNode(d);
    status = "unfixed";
    return false;
  }
  else {
    thisGraph.replaceSelectNode(d3Node, d);
    status = "fixed";
    return true;
  }

  if (thisGraph.config.debug) console.log("fixUnfixNode end");
  return status;
}

FluidGraph.prototype.replaceSelectNode = function(d3Node, d) {
  var thisGraph = this;
  d3Node.select("#nodecircle").classed(thisGraph.consts.selectedClass, true);
};

FluidGraph.prototype.removeSelectFromNode = function(d) {
  var thisGraph = this;
  thisGraph.svgNodesEnter.filter(function(node) {
    if (node.index === d.index)
    {
      node.fixed = false;
      return true;
    }
  }).select("#nodecircle").classed(thisGraph.consts.selectedClass, false);
  thisGraph.state.selectedNode = null;
};

FluidGraph.prototype.nodeOnMouseDown = function(d3Node, d) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("nodeOnMouseDown start");

  // if (d3.event.defaultPrevented) return;

  if (thisGraph.state.editedNode)
  {
    var editedNode = thisGraph.state.editedNode
  }

  if (d3Node.node() != editedNode)
  {
    thisGraph.state.mouseDownNode = d;
    thisGraph.state.svgMouseDownNode = d3Node;
  }

  if (thisGraph.config.debug) console.log("nodeOnMouseDown end");
}

FluidGraph.prototype.nodeOnMouseUp = function(d3Node, dNode) {
  //.on("mouseup",function(d){thisGraph.nodeOnMouseUp.call(thisGraph, d3.select(this), d)})
  // d3Node = d3.select(this) = array[1].<g.node>

  thisGraph = this;

  if (thisGraph.config.debug) console.log("nodeOnMouseUp start");

  // if we clicked on an origin node
  if (thisGraph.state.mouseDownNode) {
    thisGraph.state.mouseUpNode = dNode;
    // thisGraph.state.selectedNode = d;
    // if we clicked on the same node, reset vars
    if (thisGraph.state.mouseUpNode["@id"] == thisGraph.state.mouseDownNode["@id"]) {
      if (thisGraph.config.clicOnNodeAction == "flod")
      {
          // thisGraph.centerNode(dNode);
          if (thisGraph.config.allowOpenNode)
            thisGraph.openNode(d3Node, dNode);
      }
      else if (thisGraph.config.clicOnNodeAction == "options")
      {
        if (thisGraph.config.allowOpenNode)
          thisGraph.openNode(d3Node, dNode);
      }
      else if (thisGraph.config.clicOnNodeAction == "media")
      {
        var hypertext = dNode.hypertext;
        var match_url_video = hypertext.match(/(youtube|youtu|vimeo|dailymotion|kickstarter)\.(com|be)\/((watch\?v=([-\w]+))|(video\/([-\w]+))|(projects\/([-\w]+)\/([-\w]+))|([-\w]+))/)

        if (thisGraph.config.allowOpenNode)
        {
          if (match_url_video) {
            thisGraph.openVideoInNode(d3Node, dNode);
          }
          else {
            var a_closed_node_hypertext = d3Node.select("#a_closed_node_hypertext").node();
            if (a_closed_node_hypertext)
              a_closed_node_hypertext.click();
          }
        }
      }

      thisGraph.resetMouseVars();
      return;
    }
  }

  if (thisGraph.config.debug) console.log("nodeOnMouseUp end");
}

FluidGraph.prototype.nodeOnDragStart = function(d3Node, dNode) {
  //Here, "this" is the <g.node> where mouse drag
  thisGraph = this;

  if (thisGraph.config.debug) console.log("nodeOnDragStart start");

  //Prevent to close node at the second click...
  d3.event.sourceEvent.stopPropagation();

  if (thisGraph.config.activeElasticityOnDragNode)
  {
    if (!d3.event.active) thisGraph.simulation.alphaTarget(thisGraph.config.force.alphaTarget).restart();
  }

  dNode.fx = dNode.x;
  dNode.fy = dNode.y;

  //When you click on node, it dragStart on mousedown and dragEnd on mouseup
  // So, you only have to drag on close node
  if (thisGraph.state.openedNode == d3Node.node())
    return

  if (thisGraph.state.openedNode){
    var dNodeSource = thisGraph.state.openedNode.__data__;
    var dNodeTarget = dNode;

    //Don't create a link if there is already one
    var searchLinkSourceTarget = d3.select("#edge"+dNodeSource.index+"_"+dNodeTarget.index).node();
  	var searchLinkTargetSource = d3.select("#edge"+dNodeTarget.index+"_"+dNodeSource.index).node();
  }

  if (thisGraph.config.debug) console.log("nodeOnDragStart end");
}

FluidGraph.prototype.nodeOnDragMove = function(d3Node, dNode) {
  //Here, "this" is the <g.node> where mouse drag
  thisGraph = this;

  if (thisGraph.config.debug) console.log("nodeOnDragMove start");

  dNode.fx = d3.event.x;
  dNode.fy = d3.event.y;
  // console.log("nodeOnDragMove1 : $('#edit_node_textarea_label')[0]", $("#edit_node_input_hypertext")[0]);
  // console.log("nodeOnDragMove2 : d3.event.sourceEvent.target", d3.event.sourceEvent.target);
  if (d3.event.sourceEvent.target === $("#edit_node_textarea_label")[0])
    return

  if (d3.event.sourceEvent.target === $("#edit_node_input_hypertext")[0])
    return

  if (thisGraph.state.openedNode == null)
    thisGraph.MakeNodeFirstInSvgSequence(dNode);

  if (thisGraph.state.openedNode !== d3Node.node()
      && thisGraph.state.editedNode !== d3Node.node())
  {
    // if (thisGraph.state.openedNode)
    //   thisGraph.MakeNodeLastInSvgSequence(dNode);

    thisGraph.state.mouseDownNode = null;

/*
    // get coords of mouseEvent relative to svg container to allow for panning
    relCoords = d3.mouse($('svg').get(0));
    if (relCoords[0] < thisGraph.panBoundary) {
        thisGraph.panTimer = true;
        thisGraph.pan(d3Node.node(), 'left');
    } else if (relCoords[0] > ($('svg').width() - thisGraph.panBoundary)) {
        thisGraph.panTimer = true;
        thisGraph.pan(d3Node.node(), 'right');
    } else if (relCoords[1] < thisGraph.panBoundary) {
        thisGraph.panTimer = true;
        thisGraph.pan(d3Node.node(), 'up');
    } else if (relCoords[1] > ($('svg').height() - thisGraph.panBoundary)) {
        thisGraph.panTimer = true;
        thisGraph.pan(d3Node.node(), 'down');
    } else {
        try {
            clearTimeout(thisGraph.panTimer);
        } catch (e) {

        }
    }
*/
  }

  if (thisGraph.state.editedNode)
  {
    if (thisGraph.state.editedNode.__data__.index === d.index)
      return;
    else thisGraph.closeNode.call(thisGraph, "edited");
  }

  if (dNode.fixed != true) //false or undefined
  {
    thisGraph.state.draggingNode = dNode;
    thisGraph.state.draggingNodeSvg = d3Node;

    if (thisGraph.state.openedNode)
      if (thisGraph.state.draggingNodeSvg.node() === thisGraph.state.openedNode)
      {
        thisGraph.state.draggingNode = null;
        thisGraph.state.draggingNodeSvg = null;
        return
      }

    //drag node
    thisGraph.state.draggingNode.x += d3.event.dx;
    thisGraph.state.draggingNode.y += d3.event.dy;
    thisGraph.movexy();
  }

  if (thisGraph.config.debug) console.log("nodeOnDragMove end");
}

FluidGraph.prototype.nodeOnDragEnd = function(d3Node,dNode) {
  //Here, "this" is the <g.node> where mouse drag
  thisGraph = this;

  if (thisGraph.config.debug) console.log("nodeOnDragEnd start");

  if (thisGraph.config.activeElasticityOnDragNode)
  {
    if (!d3.event.active) thisGraph.simulation.alphaTarget(0).restart();
  }

  dNode.fx = null;
  dNode.fy = null;

  //When you click on node, it dragStart on mousedown and dragEnd on mouseup
  // So, you only have to drag on close node
  if (thisGraph.state.openedNode == d3Node.node())
  {
    thisGraph.state.targetNode = null;
    thisGraph.state.targetNodeSvg = null
    thisGraph.state.draggingNode = null;
    return
  }

  // if (thisGraph.state.draggingNode && thisGraph.state.openedNode == null)
  //   thisGraph.OrderNodesInSvgSequence();

/*
  if (!thisGraph.config.makeLinkByDraggingNode)
  {
    if (thisGraph.state.targetNode)
    {
      //Don't create a link if there is already one
    	var searchLinkSourceTarget = d3.select("#edge"+thisGraph.state.draggingNode.index+"_"+thisGraph.state.targetNode.index).node();
    	var searchLinkTargetSource = d3.select("#edge"+thisGraph.state.targetNode.index+"_"+thisGraph.state.draggingNode.index).node();

    	if (!searchLinkSourceTarget && !searchLinkTargetSource)
      {
        //Come back to the old position
        d3Node
            .transition()
            .duration(thisGraph.customNodes.transitionDurationComeBack)
            .attr("transform", function (d){return "translate("+d.px+","+d.py+")"})

        d3.selectAll(".link")
            .transition()
            .duration(thisGraph.customNodes.transitionDurationComeBack)
            .attr("d", thisGraph.diagonal
              .source(function(d) { return {"x":d.source.px, "y":d.source.py}; })
              .target(function(d) { return {"x":d.target.px, "y":d.target.py}; })
            )

        if (thisGraph.state.draggingNode)
        {
          thisGraph.state.draggingNode.x = thisGraph.state.draggingNode.px;
          thisGraph.state.draggingNode.y = thisGraph.state.draggingNode.py;
        }

        // console.log("nodeOnDragEnd addDataLink (draggingNode,targetNode)",
        //             thisGraph.state.draggingNode,thisGraph.state.targetNode);

        thisGraph.addDataLink(thisGraph.state.draggingNode, thisGraph.state.targetNode);
        thisGraph.drawNewLink(thisGraph.state.draggingNode, thisGraph.state.targetNode);

        // If a node is opened, add neighbour
        if (thisGraph.state.openedNode)
        {
          var d3NodeSource = d3.select(thisGraph.state.openedNode);
          var labelTypeSource = thisGraph.state.draggingNode["@type"].split(":").pop();
          var divNeighbourBoxType = d3NodeSource.select("#neighbour_type_box_"+labelTypeSource)
          thisGraph.addNeighbourInBox(divNeighbourBoxType, dNode)
        }
      }
    }
    else { // thisGraph.state.targetNode == null
      if (thisGraph.state.draggingNode)
      {
        //Re-initialisation of px and py
        thisGraph.state.draggingNode.px = thisGraph.state.draggingNode.x;
        thisGraph.state.draggingNode.py = thisGraph.state.draggingNode.y;
      }
    }

    if (thisGraph.state.targetNodeSvg)
      thisGraph.state.targetNodeSvg.select("#nodecircle").style("opacity","1");

    if (thisGraph.state.targetNode)
      thisGraph.removeSelectFromNode(thisGraph.state.targetNode);
    if (thisGraph.state.draggingNode)
      thisGraph.removeSelectFromNode(thisGraph.state.draggingNode);

    if (thisGraph.state.openedNode)
    {
      d3.select("#fo_node_neighbour").remove();
    }
  }
*/

  thisGraph.state.targetNode = null;
  thisGraph.state.targetNodeSvg = null
  thisGraph.state.draggingNode = null;

  if (thisGraph.config.activeElasticity) {
    if (dNode.fixed != true) {
      thisGraph.movexy();

      if (thisGraph.state.selectedLink) {
        thisGraph.removeSelectFromLinks();
      }

      if (thisGraph.state.selectedNode) {
        thisGraph.removeSelectFromNode(thisGraph.state.selectedNode);
      }
    }
  }

  if (thisGraph.config.debug) console.log("nodeOnDragEnd end");
}

FluidGraph.prototype.centerNode = function(d) {
  scale = d3.zoomTransform(thisGraph.svg.node());
  x = -d.x - scale.x;
  y = -d.y - scale.y;
  x = (x * scale.k) + thisGraph.svgContainer.width / 2;
  y = (y * scale.k) + thisGraph.svgContainer.height / 2;
  //console.log(scale, x, y);

  // Attention à bien choisir le bon div, pour corriger le problème de saut de déplacement !
  // var svg_g = thisGraph.svg_g; // #bg_chart
  // var bgElement = thisGraph.bgElement; // #bg_chart
  // var select_svg_g = d3.select("#svg_g"); // #svg_g
  d3.select("#svg_g").transition()
   .duration(thisGraph.customNodes.transitionDurationCenterNode)
   .attr("transform", "translate(" + x + "," + y + ")")
}

FluidGraph.prototype.deleteNodeFromD3Nodes = function(nodeIdentifier, d3Nodes) {
  var thisGraph = this;

  if (thisGraph.config.debug) console.log("deleteNodeFromD3Nodes start");

  if (d3Nodes.length) {
    //delete args or the first if not arg (console).
    if (!nodeIdentifier)
      var nodeIdentifier = d3Nodes[0]["@id"];

    var index = thisGraph.getIndexNodeFromIdentifier(d3Nodes, nodeIdentifier);
    var id = thisGraph.getIdNodeFromIdentifier(d3Nodes, nodeIdentifier);

    //delete node
    d3Nodes.splice(index, 1);

  } else {
    console.log("No node to delete !");
  }
  if (thisGraph.config.debug) console.log("deleteNodeFromD3Nodes end");

  return d3Nodes;
}
