/* *****************************
Welcome to the Fluidgraph code !
******************************** */

const FluidGraph = function (){

  //Help to assure that it's the "this" of thisGraph object
  const thisGraph = this;

  thisGraph.state = {
    selectedNode : null,
    selectedLink : null,
    mouseDownNode : null,
    mouseDownLink : null,
    svgMouseDownNode : null,
    mouseUpNode : null,
    lastKeyDown : -1,
    editedNode : null,
    editedIndexNode : null,
    openedNode : null,
    editedLinkLabel : null,
    dragStarted : null,
    draggingNode : null,
    draggingNodeSvg : null,
    targetNode : null,
    targetNodeSvg : null,
    focusMode : null,
    filterTypeMode : false,
  }

  thisGraph.bgElement = null;
  thisGraph.svg = null;
  thisGraph.svg_g_g_rect = null;
  thisGraph.svg_g_zoom = null;
  thisGraph.currentTransformZoom = null;

//    thisGraph.graphName = thisGraph.consts.UNTILTED_GRAPH_NAME;
  thisGraph.openedGraph = null;
  thisGraph.selectedGraphName = null;
  thisGraph.ldpGraphName = null;
  thisGraph.selectedLdpGraphName = null;
  thisGraph.mockData0 = null;
  thisGraph.listOfLocalGraphs = [];
  thisGraph.listOfExtenalGraphs = [];
  thisGraph.graphToDeleteName = null;
  thisGraph.triples = {};
  thisGraph.d3Data = {};
  thisGraph.d3Data.nodes = [];
  thisGraph.d3Data.edges = [];
  thisGraph.d3DataFiltered = {};
  thisGraph.d3DataFiltered.nodes = [];
  thisGraph.d3DataFiltered.edges = [];
  thisGraph.svgNodesEnter = [];
  thisGraph.svgEdgesEnter = [];
  thisGraph.svgEdgeLabelEnter = [];

  thisGraph.searchData = [];
  thisGraph.jsonObjContext = {};

  thisGraph.panTimer = null;
  thisGraph.zoomListener = null;
  thisGraph.translateX = null;
  thisGraph.translateY = null;

  thisGraph.dataFilterTypeNodeOption = [];
  thisGraph.dataFilterTypeEdgeOption = [];
  thisGraph.dataFilterDisplayNodesAlone = [
  {
    name: 'Tous',
    value: 'all',
    text: 'Tous',
  },
  {
    name: 'Noeuds et liens',
    value: 'nodesEdgesOnly',
    text: 'Noeuds et liens',
  },
  {
    name: 'Noeuds isolés',
    value: 'nodesOnly',
    text: 'Noeuds isolés',
  }];

  thisGraph.statisticsD3Data = {
    "nodes" : 0,
    "edges" : 0,
// Classes
    "pair:person" : 0,
    "pair:organization" : 0,
    "pair:project" : 0,
    "pair:concept" : 0,
    "pair:resource" : 0,
    "pair:skill" : 0,
    "pair:theme" : 0,
    "pair:thema" : 0,
    "pair:organizationtype" : 0,
    "pair:event" : 0,
    "pair:place" : 0,

// Predicats
    "pair:hasInterest" : 0,
    "pair:involves" : 0,
    "pair:offeredBy" : 0,
    "pair:involvedIn" : 0,
    "pair:hasMember" : 0,
    "pair:offers" : 0,
    "pair:interestOf" : 0,
    "pair:memberOf" : 0,
    "pair:hostedIn" : 0,
    "pair:hasSubjectType" : 0,
    "pair:hasTopic" : 0,
    "pair:partnerOf" : 0,
    "pair:typeOfSubject" : 0,
    "pair:involves" : 0,
    "pair:membershipActor" : 0,
    "pair:supportBy" : 0,
    "pair:hasSkill" : 0,
  };

  thisGraph.statisticsD3DataFiltered = {
    "nodes" : 0,
    "edges" : 0,
// Classes
    "pair:person" : 0,
    "pair:organization" : 0,
    "pair:project" : 0,
    "pair:concept" : 0,
    "pair:resource" : 0,
    "pair:skill" : 0,
    "pair:theme" : 0,
    "pair:thema" : 0,
    "pair:organizationtype" : 0,
    "pair:event" : 0,
    "pair:place" : 0,

// Predicats
    "pair:hasInterest" : 0,
    "pair:involves" : 0,
    "pair:offeredBy" : 0,
    "pair:involvedIn" : 0,
    "pair:hasMember" : 0,
    "pair:offers" : 0,
    "pair:interestOf" : 0,
    "pair:memberOf" : 0,
    "pair:hostedIn" : 0,
    "pair:hasSubjectType" : 0,
    "pair:hasTopic" : 0,
    "pair:partnerOf" : 0,
    "pair:typeOfSubject" : 0,
    "pair:involves" : 0,
    "pair:membershipActor" : 0,
    "pair:supportBy" : 0,
    "pair:hasSkill" : 0,
    "pair:affiliatedBy" : 0,
    "pair:affiliates" : 0,
  };

}

FluidGraph.prototype.consts =  {
  selectedClass: "selected",
  connectClass: "connect-node",
  circleGClass: "conceptG",
  graphClass: "graph",
  BACKSPACE_KEY: 8,
  DELETE_KEY: 46,
  ENTER_KEY: 13,
  nodeRadius: 50,
  OPENED_GRAPH_KEY: "openedGraph",
  UNTILTED_GRAPH_NAME: "Untilted",
};

FluidGraph.prototype.fetchConfig = async function(fileName){
  // Récupération du contenu des fichiers de config de FluidGraph
  try {
    let response = await fetch(fileName);
    if (response.ok){
      let config = await response.json();
      return config;
    }
    else {
      console.error(fileName+" - Erreur lors de la récupération de la config : ", response.status)
      $("#alertModalConfig").modal('show');
    }
  } catch (e){
    console.log(fileName+" - ", e)
    $("#alertModalConfig").modal('show');
  }
}

FluidGraph.prototype.menuInitialisation = function(){
  const thisGraph = this;
  if (thisGraph.config.debug) console.log("menuInitialisation start");

  $('#focusContextNodeOff').hide();

  $('#menuTitle').append(thisGraph.config.menuTitle);

  if (thisGraph.config.curvesEdges)
    $('#curvesEdgesCheckbox').checkbox('check');
  else
    $('#curvesEdgesCheckbox').checkbox('uncheck');

  if (thisGraph.config.editGraphMode)
    $('#editGraphModeCheckbox').checkbox('check');
  else
    $('#editGraphModeCheckbox').checkbox('uncheck');

  if (thisGraph.config.clicOnNodeAction == "flod")
    $('#displayFlodCheckbox').checkbox('check');
  else
    $('#displayFlodCheckbox').checkbox('uncheck');

  if (thisGraph.config.displayFlodPanel)
  {
    $('#displayFlodPanel').checkbox('check');
    $('#flodPanelId').show();
  }
  else
  {
    $('#displayFlodPanel').checkbox('uncheck');
    $('#flodPanelId').hide();
  }

  if (thisGraph.config.force.enabled)
    $('#activeForceCheckbox').checkbox('check');
  else
    $('#activeForceCheckbox').checkbox('uncheck');

  if (thisGraph.config.activeElasticity)
    $('#activeElasticityCheckbox').checkbox('check');
  else
    $('#activeElasticityCheckbox').checkbox('uncheck');

  if (thisGraph.customNodes.displayIndex)
    $('#displayIndexCheckbox').checkbox('check');
  else
    $('#displayIndexCheckbox').checkbox('uncheck');

  if (thisGraph.customNodes.displayFilterEdge)
  {
    $('#displayFilterEdgeCheckbox').checkbox('check');
    $('#filterEdgesSegmentId').show();
  }
  else
  {
    $('#displayFilterEdgeCheckbox').checkbox('uncheck');
    $('#filterEdgesSegmentId').hide();
  }

  if (thisGraph.config.typeServer == 'local')
  {
    $('#typeServerCheckbox').checkbox('uncheck');
    $('#graphNameSegment').show();
  }
  else
  {
    $('#typeServerCheckbox').checkbox('check');
    $('#graphNameSegment').hide();
  }

  if (thisGraph.config.debug) console.log("menuInitialisation end");
}

FluidGraph.prototype.control = async function(){
  const thisGraph = this;
  if (thisGraph.config.debug) console.log("control start");

  $('#flod_panel_content_label').text(thisGraph.config.firstLabelFlodPanel)

  $('#logoSemappsId')
  .removeClass('tiny')
  .addClass(thisGraph.config.logoType)
  .attr('src', 'img/'+thisGraph.config.logoName);

  $('#sidebarId').css('background-color', thisGraph.config.menuColor);
  $('#topAttachedMenuId').css('background-color', thisGraph.config.menuColor);
  $('#displayGraphButtonId').css('background-color', thisGraph.config.sidebarButtonColor);
  $('#loadGraphButtonId').css('background-color', thisGraph.config.sidebarButtonColor);
  $('#paramsGraphButtonId').css('background-color', thisGraph.config.sidebarButtonColor);
  $('#forceAlphaId').css('background-color', thisGraph.config.sidebarButtonColor);

  if (!thisGraph.config.displayStatistics)
    $('#statisticSegmentId').hide();

  $('.ui.sidebar')
  .sidebar({
      context: $('.bottom.segment')
    })
  .sidebar('attach events', '.menu .item')
  .sidebar('setting', 'transition', 'overlay')
  .sidebar('setting', 'dimPage', false)

  $('#loadGraphButtonId').click( async () => {
    thisGraph.d3Data = {};
    thisGraph.d3Data.nodes = [];
    thisGraph.d3Data.edges = [];

    await thisGraph.chooseLoaderAndLoadD3Data();
    await thisGraph.initializeFiltersInMenu();

    if (thisGraph.config.keepMenuOpen)
      $('.ui.sidebar').sidebar('toggle');
  });

  $('#displayGraphButtonId').click( async () => {
    await thisGraph.initializeAndDisplayGraph();
  });

  $('#paramsGraphButtonId').click( () => {
    $('#settingsModal')
      .modal('show');
  });

  $('#parametersApproveButtonId').click( async () => {
    await thisGraph.initializeAndDisplayGraph();
  });

  $('#alertApproveButtonId').click( function() {
    thisGraph.config.typeServer = "home";
    thisGraph.chooseLoaderAndLoadD3Data();
    thisGraph.displayGraph();
  });

  $('#typeServerCheckbox').checkbox({
    onChecked:function() {
      thisGraph.config.typeServer = "external";
      $('#graphNameSegment').hide();
    },
    onUnchecked: function() {
      thisGraph.config.typeServer = "local";
      $('#graphNameSegment').show();
    },
  });

  $('#dropdown-filter-nodesAlone').dropdown('change values',thisGraph.dataFilterDisplayNodesAlone);
  $('#dropdown-filter-nodesAlone').dropdown('set selected',thisGraph.config.displayNodesAlone);
  $('#dropdown-filter-nodesAlone').dropdown('setting', {
    'onChange' : function(value, text, $choice) {
        thisGraph.config.displayNodesAlone = value;
      },
  });

  $('#activeElasticityCheckbox').checkbox({
    onChecked: function() {
      thisGraph.config.activeElasticity = true;
      $('#forceAlphaId').show()
    },
    onUnchecked: function() {
      thisGraph.config.activeElasticity = false;
      $('#forceAlphaId').hide()
    }
  });

  if (thisGraph.config.activeElasticity)
    $('#forceAlphaId').show()
  else
    $('#forceAlphaId').hide()

  $('#activeElasticityOnDragCheckbox').checkbox({
    onChecked: function() {
      thisGraph.config.activeElasticityOnDragNode = true;
      $('#forceAlphaId').show()
    },
    onUnchecked: function() {
      thisGraph.config.activeElasticityOnDragNode = false;
      $('#forceAlphaId').hide()
    }
  });

  $('#displayFlodCheckbox').checkbox({
    onChecked:function() {
      thisGraph.config.clicOnNodeAction = "flod"
    },
    onUnchecked: function() {
      thisGraph.config.clicOnNodeAction = "options"
    }
  });

  $('#displayFlodPanelCheckbox').checkbox({
    onChecked: function() {
      thisGraph.config.displayFlodPanel = true;
      $('#flodPanelId').show();
    },
    onUnchecked: function() {
      thisGraph.config.displayFlodPanel = false;
      $('#flodPanelId').hide();
    }
  });

  $('#curvesEdgesCheckbox').checkbox({
    onChecked:function() {
      thisGraph.config.curvesEdges = true;
    },
    onUnchecked: function() {
      thisGraph.config.curvesEdges = false;
    },
  });

  $('#displayIndexCheckbox').checkbox({
    onChecked: function() {
      thisGraph.customNodes.displayIndex = true;
    },
    onUnchecked: function() {
      thisGraph.customNodes.displayIndex = false;
    }
  });

  $('#displayFilterEdgeCheckbox').checkbox({
    onChecked: function() {
      thisGraph.customNodes.displayFilterEdge = true;
      $('#filterEdgesSegmentId').show();
    },
    onUnchecked: function() {
      thisGraph.customNodes.displayFilterEdge = false;
      $('#filterEdgesSegmentId').hide();
    }
  });

  $('#chargeStrengthSliderOutputId').val(thisGraph.config.force.charge.strength);
  $('#chargeStrengthSliderInputId').val(thisGraph.config.force.charge.strength);
  if (thisGraph.config.force.chargeStrengthSlider)
    $('#chargeStrengthSliderId').show();
  else
    $('#chargeStrengthSliderId').hide();

  $('#collideStrengthSliderOutputId').val(thisGraph.config.force.collide.strength);
  $('#collideStrengthSliderInputId').val(thisGraph.config.force.collide.strength);
  if (thisGraph.config.force.collideStrengthSlider)
    $('#collideStrengthSliderId').show();
  else
    $('#collideStrengthSliderId').hide();

  $('#linkDistanceSliderOutputId').val(thisGraph.config.force.link.distance);
  $('#linkDistanceSliderInputId').val(thisGraph.config.force.link.distance);
  if (thisGraph.config.force.linkDistanceSlider)
    $('#linkDistanceSliderId').show();
  else
    $('#linkDistanceSliderId').hide();

  $('#linkStrengthSliderInputId').val(thisGraph.config.force.link.strength);
  $('#linkStrengthSliderOutputId').val(thisGraph.config.force.link.strength);
  if (thisGraph.config.force.linkStrengthSlider)
    $('#linkStrengthSliderId').show();
  else
    $('#linkStrengthSliderId').hide();

  if (thisGraph.config.debug) console.log("control end");
}

//Create a balise SVG with events
FluidGraph.prototype.initSvgContainer = function(firstBgElement){
  thisGraph = this;
  if (thisGraph.config.debug) console.log("initSgvContainer start");

  // listen for key events
  d3.select(window).on("keydown", function(){
      thisGraph.bgKeyDown.call(thisGraph);
  })
  .on("keyup", function(){
      thisGraph.bgKeyUp.call(thisGraph);
  });

  let element = firstBgElement.substring(1);

  let menuHeight = 60 // $("#topAttachedMenuId").outerHeight()+1; --> Ne fonctionne pas toujours...
  thisGraph.svgContainer = {
    width : window.innerWidth,
    height : window.innerHeight-menuHeight,
  }

  if (thisGraph.config.bgElementType == "simple")  {

    thisGraph.svg = d3.select(firstBgElement)
          .append("svg")
          .attr("width", thisGraph.svgContainer.width)
          .attr("height", thisGraph.svgContainer.height)
          .append('g')
          .attr('id', "bg_"+element)
  }
  else  {  //panzoom
    thisGraph.zoomListener = d3.zoom()
      .scaleExtent([thisGraph.config.zoomInMin, thisGraph.config.zoomOutMax])
      .on("zoom", thisGraph.rescale);

    thisGraph.svg = d3.select(firstBgElement)
          .append("svg")
          .attr("width", thisGraph.svgContainer.width)
          .attr("height", thisGraph.svgContainer.height)
          .call(thisGraph.zoomListener)

    thisGraph.svg_g = thisGraph.svg
      .append('g')
      .attr('id', "svg_g")
      .on("dblclick", function(){
          if (thisGraph.config.newNodeWithDoubleClickOnBg)
            thisGraph.addNodeOnBg.call(this, thisGraph);
      })

      //thisGraph.svg_g =  thisGraph.svg_g
        .append('g')
        .attr('id', "bg_"+element)

        // .on("mouseover", function(d){
        //   // console.log("mouseover sur bg")
        //   d3.event.stopPropagation();
        // })
        // .on("mouseout", function(d){
        //   // console.log("mouseout sur bg")
        //   d3.event.stopPropagation();
        // })


      thisGraph.svg_g_g_rect = thisGraph.svg_g
              .append('rect')
              .attr('x', -thisGraph.svgContainer.width*10)
              .attr('y', -thisGraph.svgContainer.height*10)
              .attr('width', thisGraph.svgContainer.width*20)
              .attr('height', thisGraph.svgContainer.height*20)
              .attr('fill', thisGraph.config.backgroundColor)
              .on("mousedown", function(d){
                  thisGraph.bgOnMouseDown.call(thisGraph, d)
              })
              .on("mousemove", function(d){
                  thisGraph.bgOnMouseMove.call(thisGraph, d)
              })
              .on("click", function(d){ // On V4, click replace mouseup
              })
}

  thisGraph.bgElement = d3.select("#bg_"+element);

  if (thisGraph.config.debug) console.log("initSgvContainer end");
}
